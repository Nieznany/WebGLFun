var turtle = {
    x           : 0,
    y           : 0,
    angle       : 0,
    minX        : -1005,
    maxX        : 1005,
    minY        : -1005,
    maxY        : 1005,
    setCanvas   : function(canvas) {
        this.width = canvas.width;
        this.height = canvas.height;
        this.ctx = canvas.getContext("2d");
    },
    move        : function() {
        this.ctx.moveTo(Math.round(((this.x - this.minX)/(this.maxX-this.minX))*this.width),
                        this.height - Math.round(((this.y - this.minY)/(this.maxY-this.minY))*this.height));
    },
    line      : function() {
        this.ctx.lineTo(Math.round(((this.x - this.minX)/(this.maxX-this.minX))*this.width),
                        this.height - Math.round(((this.y - this.minY)/(this.maxY-this.minY))*this.height));
    },
    forward     : function(distance) {
        this.ctx.beginPath();
        this.move();
        this.x = Math.round(this.x + Math.sin(this.angle * Math.PI / 180) * distance);
        this.y = Math.round(this.y + Math.cos(this.angle * Math.PI / 180) * distance);
        this.line();
	    this.ctx.stroke();
    },
    left        : function(alpha) {
        this.angle -= alpha;
    },
    right       : function(alpha) {
        this.angle += alpha;
    },
    color       : function(col) {
        this.ctx.strokeStyle = col;
    },
    clear       : function() {    
        this.ctx.clearRect(0, 0, this.width, this.height);
    }
}
    
