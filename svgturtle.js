var turtle = {
    x           : 0,
    y           : 0,
    prevX       : 0,
    prevY       : 0,
    angle       : 0,
    minX        : -1005,
    maxX        : 1005,
    minY        : -1005,
    maxY        : 1005,
    col         : "black",
    setSvg      : function(svg) {
        this.width = svg.width.baseVal.value;
        this.height = svg.height.baseVal.value;
        this.svg = svg;
    },
    move        : function() {
        this.prevX = Math.round(((this.x - this.minX)/(this.maxX-this.minX))*this.width);
        this.prevY = this.height - Math.round(((this.y - this.minY)/(this.maxY-this.minY))*this.height);
    },
    line      : function() {
        this.svg.innerHTML = this.svg.innerHTML + '<line x1="'+this.prevX+'" y1="'+this.prevY+
        '" x2="'+Math.round(((this.x - this.minX)/(this.maxX-this.minX))*this.width)+
        '" y2="'+(this.height - Math.round(((this.y - this.minY)/(this.maxY-this.minY))*this.height))+
        '" style="stroke:'+this.col+';stroke-width:2" />\n';
    },
    forward     : function(distance) {
        this.move();
        this.x = Math.round(this.x + Math.sin(this.angle * Math.PI / 180) * distance);
        this.y = Math.round(this.y + Math.cos(this.angle * Math.PI / 180) * distance);
        this.line();
    },
    left        : function(alpha) {
        this.angle -= alpha;
    },
    right       : function(alpha) {
        this.angle += alpha;
    },
    color       : function(col) {
        this.col = col;
    },
    clear       : function() {    
        this.svg.innerHTML = '\n'
    }
}
    
