var multiplyByVector = function(M, v) {
    var w = new Array(3);
    for(var i = 0; i < 3; i++)
    {
        w[i] = 0
        for(var k = 0; k < 3; k++)
        {
            w[i] += M[i][k]*v[k];
        }
    }
    return w;
}

var multiply = function(M1, M2) {
    var M = new Array(3);
    
    for(var i = 0; i < M.length; i++)
    {
        M[i] = new Array(3);
    }
    for(var i = 0; i < 3; i++)
    {
        for(var j = 0; j < 3; j++)
        {
            M[i][j] = 0;
            for(var k = 0; k < 3; k++)
            {
                M[i][j] += M1[i][k]*M2[k][j];
            }
        }
    }
    return M;
}

var transpose = function(M) {
    var w = new Array(3);
    for(var i = 0; i < 3; i++)
    {
        w[i] = new Array(3);
        for(var k = 0; k < 3; k++)
        {
            w[i][k] = M[k][i];
        }
    }
    return w;
}

var subtract = function(v1, v2) {
    var w = new Array(3);
    for(var i = 0; i < 3; i++)
    {
        w[i] = v1[i] - v2[i];
    }
    return w;
}

var add = function(v1, v2) {
    var w = new Array(3);
    for(var i = 0; i < 3; i++)
    {
        w[i] = v1[i] + v2[i];
    }
    return w;
}

var wfengine = {
    minX        : -1,
    maxX        : 1,
    minY        : -1,
    maxY        : 1,
    
    camPos      : [0, 0, 0],
    e           : [0, 0, 2],
    
    rotateM     : [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    rotateMT    : [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    M           : [[1, 0, 0], [0, 1, 0], [0, 0, 1/2]],
    
    setCanvas   : function(canvas) {
        this.width = canvas.width;
        this.height = canvas.height;
        this.ctx = canvas.getContext("2d");
    },
    
    
    canDrawLine : function(p1, p2) {
        if(p1[2] <= 0 && p2[2] <= 0)
        {
            return;
        }
        if(p1[2] <= 0)
        {
            p1[0] = p2[0] + 100*(p2[0] - p1[0])
            p1[1] = p2[1] + 100*(p2[1] - p1[1])
        }
        if(p2[2] <= 0)
        {
            p2[0] = p1[0] + 100*(p1[0] - p2[0])
            p2[1] = p1[1] + 100*(p1[1] - p2[1])
        }
        
        this.ctx.moveTo(Math.round(((p1[0] - this.minX)/(this.maxX-this.minX))*this.width),
                        this.height - Math.round(((p1[1] - this.minY)/(this.maxY-this.minY))*this.height));
        this.ctx.lineTo(Math.round(((p2[0] - this.minX)/(this.maxX-this.minX))*this.width),
                        this.height - Math.round(((p2[1] - this.minY)/(this.maxY-this.minY))*this.height));
    },
    
    clear       : function() {    
        this.ctx.clearRect(0, 0, this.width, this.height);
    },
    
    project     : function(a) {    
        var d = subtract(a, this.camPos);
        var f = multiplyByVector(this.M, d);
        if(f[2] != 0)
        {
            f[0] = f[0]/f[2];
            f[1] = f[1]/f[2];
        }
        return f;
    },
    
    draw        : function(a) {
        a = a.map(this.project, this);   
        for(var i = 0; i + 1 < a.length; i += 2)
        {
            this.canDrawLine(a[i], a[i+1]);
        }
        
    },
    
    render      : function() {
        this.ctx.stroke();
        this.ctx.beginPath()

    },
    
    moveCam     : function(v) {
        this.camPos = add(this.camPos, multiplyByVector(this.rotateMT, v));
    },
    
    rotateCam   : function(v) {
        this.rotateM = multiply([[Math.cos(v[2]), Math.sin(v[2]), 0], [-Math.sin(v[2]), Math.cos(v[2]), 0],[0, 0, 1]],this.rotateM);
        this.rotateM = multiply([[Math.cos(v[1]), 0, -Math.sin(v[1])],[0, 1, 0], [Math.sin(v[1]), 0, Math.cos(v[1])]],this.rotateM);
        this.rotateM = multiply([[1, 0, 0], [0, Math.cos(v[0]), Math.sin(v[0])],[0, -Math.sin(v[0]), Math.cos(v[0])]],this.rotateM);
        this.rotateMT = transpose(this.rotateM);
        this.M = multiply([[1, 0, this.e[0]/this.e[2]], [0, 1, this.e[1]/this.e[2]], [0, 0, 1/this.e[2]]], this.rotateM);
    },
    
    color       : function(color) {
        this.ctx.stroke();
        this.ctx.beginPath();
        this.ctx.strokeStyle=color;
    }
}
