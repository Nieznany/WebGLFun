class Rectangle {
    constructor(x, y, z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        
        var corners = [];
        
        for(var i = -1; i <= 1; i += 2)
        {
            for(var j = -1; j <= 1; j += 2)
            {
                for(var k = -1; k <= 1; k += 2)
                {
                    corners.push([x + i, y + j, z + k]);
                }
            }
        }
        
        this.drawable = []
        
        for(var i = 0; i < 8; ++i)
        {
            for(var j = i; j < 8; ++j)
            {
                var count = 0;
                for(var k = 0; k < 3; ++k)
                {
                    if (corners[i][k] == corners[j][k])
                    {
                        ++count;
                    }
                }
                if (count == 2)
                {
                    this.drawable.push(corners[i])
                    this.drawable.push(corners[j])
                }
            }
        }
    }
    
    detectCollision(x, y, z)
    {
        if(this.x - 1 > x)
        {
            return false;
        }
        if(this.y - 1 > y)
        {
            return false;
        }
        if(this.z - 1 > z)
        {
            return false;
        }
        if(x > this.x + 1)
        {
            return false;
        }
        if(y > this.y + 1)
        {
            return false;
        }
        if(z > this.z + 1)
        {
            return false;
        }
        return true;
    }
}

var border = new Rectangle(0, 0, 0)

var turtle = {
    pos         : [0,0,0],
    drawable    : [],
    rotateM     : [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    forward     : function(distance) {
        var newPos = add(this.pos, multiplyByVector(this.rotateM, [0, 0, distance]))
        if(border.detectCollision(newPos[0], newPos[1], newPos[2]))
        {
            this.drawable.push(this.pos);
            this.drawable.push(newPos);
            this.pos = newPos;
        }
    },
    left        : function(alpha) {
        alpha = alpha/180.0*Math.PI
        this.rotateM =multiply(this.rotateM, [[Math.cos(alpha), 0, -Math.sin(alpha)],[0, 1, 0], [Math.sin(alpha), 0, Math.cos(alpha)]]);

    },
    right       : function(alpha) {
        alpha = alpha/180.0*Math.PI
        this.rotateM =multiply(this.rotateM, [[Math.cos(alpha), 0, Math.sin(alpha)],[0, 1, 0], [-Math.sin(alpha), 0, Math.cos(alpha)]]);
    },
    up          : function(alpha) {
        alpha = alpha/180.0*Math.PI
        this.rotateM =multiply(this.rotateM, [[1, 0, 0], [0, Math.cos(alpha), Math.sin(alpha)],[0, -Math.sin(alpha), Math.cos(alpha)]]);
    },
    down        : function(alpha) {
        alpha = alpha/180.0*Math.PI
        this.rotateM =multiply(this.rotateM, [[1, 0, 0], [0, Math.cos(alpha), -Math.sin(alpha)],[0, Math.sin(alpha), Math.cos(alpha)]]);
    },
}
    
