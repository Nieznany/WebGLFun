class Rectangle {
    constructor(x, y, z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        
        var corners = [];
        
        for(var i = -1; i <= 1; i += 2)
        {
            for(var j = -1; j <= 1; j += 2)
            {
                for(var k = -1; k <= 1; k += 2)
                {
                    corners.push([x + i, y + j, z + k]);
                }
            }
        }
        
        this.drawable = []
        
        for(var i = 0; i < 8; ++i)
        {
            for(var j = i; j < 8; ++j)
            {
                var count = 0;
                for(var k = 0; k < 3; ++k)
                {
                    if (corners[i][k] == corners[j][k])
                    {
                        ++count;
                    }
                }
                if (count == 2)
                {
                    this.drawable.push(corners[i])
                    this.drawable.push(corners[j])
                }
            }
        }
    }
    
    detectCollision(x, y, z)
    {
        if(this.x - 1 > x)
        {
            return false;
        }
        if(this.y - 1 > y)
        {
            return false;
        }
        if(this.z - 1 > z)
        {
            return false;
        }
        if(x > this.x + 1)
        {
            return false;
        }
        if(y > this.y + 1)
        {
            return false;
        }
        if(z > this.z + 1)
        {
            return false;
        }
        return true;
    }
}

var rectangles = [];

var goal = new Rectangle(0, 0, 20);

var collisions = function()
{
    for(var i = 0; i < 100; ++i)
    {
        if(rectangles[i].detectCollision(player.x, player.y, player.z))
        {
            return true;
        }
    }
    return false
}

var player = {
    x:  0,
    y:  0,
    z:  0,
    
    move: function(v)
    {
        wfengine.moveCam(v)
        player.x = wfengine.camPos[0]
        player.y = wfengine.camPos[1]
        player.z = wfengine.camPos[2]
        
        if(goal.detectCollision(player.x, player.y, player.z))
        {
            console.log("WIN")
            goal = new Rectangle( Math.floor((Math.random() * 30) -14), Math.floor((Math.random() * 30) - 14   ), Math.floor((Math.random() * 30) - 9))
        }
        
        if(collisions())
        {
            wfengine.moveCam([-v[0], -v[1], -v[2]])
            player.x = wfengine.camPos[0]
            player.y = wfengine.camPos[1]
            player.z = wfengine.camPos[2]
        }
        

    }
}



for(var i = 0; i < 100; ++i)
{
    var rect = new Rectangle( Math.floor((Math.random() * 30) -14), Math.floor((Math.random() * 30) - 14   ), Math.floor((Math.random() * 30) - 9))
    while(rect.detectCollision(player.x, player.y, player.z))
    {
        rect = new Rectangle( Math.floor((Math.random() * 30) -14), Math.floor((Math.random() * 30) - 14   ), Math.floor((Math.random() * 30) - 9))
    } 
    rectangles.push(rect)
}

